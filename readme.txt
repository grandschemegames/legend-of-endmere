
Legend of Endmere comes with a copy of the current game and the Japanese lisenced version of RPG Maker 2003

The game will not work without this version of RPG Maker  2003

Installation instructions for Legend of Endmere:

1. Open Rm2k3_RPG File

2. Install Rm2k3 application using rm2k3-install

3. After the completed installation, to launch the game, simply click on RPG_RT


Game Controls:

Use the Arrow Keys to move

Z button to accept

X button to cancel and access the menu

"1" key to access the leveling menu

