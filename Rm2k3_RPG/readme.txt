
---------------------------------------------------
RPG Maker 2003 English Translation by RPG Advocate
---------------------------------------------------

+-----------------------+
| Contents of this File |
+-----------------------+

1. Version Information
2. Installing the Software
3. If the In-Game Fonts Don't Look Right...
4. Troubleshooting
5. Known Issues with the Program
6. Known Issues with the Translation
7. About the Fantasy Picture Book
8. Regarding "Hacks"
9. Support
10. Special Thanks

+---------------------+
| Version Information |
+---------------------+

Initial Release Version:    Version 1.04      3.18.2003
Current Release Version:    Version 1.08      10.1.2003
Help File Version:          Version 1.05      5.27.2003

+-------------------------+
| Installing the Software |
+-------------------------+

To install the software, simply run the "rm2k3-install.exe" file included
in this archive and follow the instructions.  Both the software and the
RTP (runtime package) will be installed.  Once installed, run "RPG2003.exe"
to begin making your game.

NOTICE: If you were using another translation before acquiring mine, the RPG_RT.exe
files associated with your existing projects will not be updated automatically
when you install my translation.  The process for updating the RPG_RT.exe files is
as follows:

1. Once you install my translation, create a new project.

2. Go to that project's folder, select the RPG_RT.exe file, and select "copy"

3. Go to your existing project's folder, and paste that RPG_RT.exe file into
your project, overwriting the old one.

4. You can then delete the new project's folder.

IMPORTANT: This piece of software depends heavily on the relative locations
of files.  In order to ensure proper operation of the software, don't move
files around once the software is installed.  If you want to move it to
a different directory, you're better off deleting and reinstalling.

+------------------------------------------+
| If the In-Game Fonts Don't Look Right... |
+------------------------------------------+

If the in-game fonts appear squishy and the letters run together, try the following steps,
in order, to solve the problem:

1. Install the "msgothi0.ttc" and "msmincho.ttc" fonts by copying them from the fonts
directory included in the installation to the  \windows\fonts directory and double-clicking on them.  See if the fonts look correct now.

2. If the step above didn't work, install the "RM2000.fon" and "RMG2000.fon" font files
into windows\fonts as described above (note that installing these files isn't necessary if you already have RPG Maker 2000 installed), then copy the "RPG_RT.exe.dat" file included in
the fonts directory over the one in the main application directory (the same directory as the RPG2003.exe file).  Create a new project and see if the fonts look correct in that new project.

TIP: I have been told that downloading Japanese language support for Internet Explorer makes the fonts used for method #1 work reliably.  Try it and see.

NOTE: If you have to use method #2 above to fix the fonts, projects you created before performing the fix will not be fixed.  You must copy the RPG_RT.exe file from the new project over the one in your existing project(s).

+-----------------+
| Troubleshooting |
+-----------------+

Problem: When trying to create a project for the first time, you get a "Project Directory could not be Created" error.

Solution: The default project path is pointing to a folder that doesn't exist.  Point the project path to a folder on your machine and this problem should go away.

Problem: Strange errors occur when trying to import an RPG Maker 2000 game.

Solution: Importing is far from perfect due to file incompatibility issues.  First, make sure you have a system graphic set in the system tab.  Second, make sure you have a system 2 graphic set in the system2 tab.  Third, make sure you have a battle background to be displayed.  Fourth, make sure you've set up battle sprites for each character (Note that this is not done for you if you import a game!)

Problem: When I play someone else's game, the font is messed up

Solution: If the game author used the opposite font fix, the fonts will appear squished.  Just copy an RPG_RT.exe file from one of your projects over theirs.  The font will then be readable.

+-------------------------------+
| Known Issues with the Program |
+-------------------------------+

Following are known issues that were present in the original
Japanese version of the program.  They are not bugs that arose during
translation:

- Sometimes when MIDI music is playing, only the piano instrument will
play.  Enterbrain has officially acknowledged this bug and is
attempting to fix it.

- Error-checking in RPG_RT.exe is flaky.  If you give the party
an invalid skill or item, RPG_RT.exe will not show an error until
you open the item list or skill list.  Enterbrain has not officially
acknowledged this bug at this time.

- Only the first line of a comment shows up in green.  Enterbrain has
stated that this will not be fixed for the time being.

- On some machines, the game will freeze when you try to run a Test
Battle.  To get around this, just test battles within a game as
opposed to using the Test Battle command.

- Sometimes when exiting a Test Play, an error message will
appear.  While annoying, this error is harmless and should not
cause distress.

+-----------------------------------+
| Known Issues with the Translation |
+-----------------------------------+

Following are known issues that arose as a result of translating the
program and were not present in the Japanese version:

- Support for characters with diacritical marks is lacking.  
You will be able to view and enter such characters in only
some fields in the editor.  These characters do not work at all
in in-game messages due to a byte parsing issue with RPG_RT.exe.  If
you know how to fix this without changing fonts, contact me.

- Due to fonts being different between machines, the "enter message"
input area may allow you to enter five lines on some machines.  No matter
how many lines the input area allows, ONLY FOUR LINES WILL BE SHOWN IN-GAME!
Please don't enter more than four lines per message.

+--------------------------------+
| About the Fantasy Picture Book |
+--------------------------------+

The Fantasy Picture Book is part of the Japanese release of RPG Maker 2003.
It gives information about the mythological background of the RTP monsters.
It was originally presented as a HTML Help (.chm) file, but here it is
presented as a collection of HTML files and PNG images.  They need to
be kept in the same directory for the links and images to work correctly.

If you're not interested in it, you can simply delete the folder 
without problems.  It is not required for RPG Maker 2003's operation.

+-------------------+
| Regarding "Hacks" |
+-------------------+

If you have used another RPG Maker 2003 translation, you may have
noticed that there were "hacks" present, such as a claim of being
able to use 9999 frames in a battle animation, or being able to edit
your game while test-playing it.

This translation contains no such hacks, and for good reason.

As for "maximum value" hacks, it may sound impressive to boast that
you can make a 9999-frame battle animation, but most people haven't
actually tried this.  It will crash the editor without exception,
because simply changing the MaxValue property to 9999 in the Delphi
form associated with battle animations is not sufficient to make it
so.  This requires changes that would take an amazing feat of skill
to pull off without access to the source code.

Regarding the 'edit while you test play" hack, Enterbrain made it so
you cannot alter game data while test-playing it for a good reason.
Ensuring the game data doesn't change while it's being accessed removes
the possibility that the data will be accessed during a write operation to
your project, which could cause nasty assertion errors.

+---------+
| Support |
+---------+

I will not provide extensive support for this translation.  Most problems
users experience are a result of PC environment issues or improper installations.
However, I do read many RPG Maker boards.  If a bug crops up repeatedly, I will
investigate it.

+----------------+
| Special Thanks |
+----------------+

The following people have contributed something meaningful to this project,
and deserve recognition for their contributions:

Trihan: Beta tester and help file author.
Blue Lander: Fixed the Enter Hero Name function in v1.06 and above.
CodeMason: For help in resolving the unicode parsing issue.
Ryethe: For help on formatting issues with regard to making 
RPG Maker 2003 presentable for an English-speaking audience.
MagnusRM2K: For offering to promote this translation on his site.




